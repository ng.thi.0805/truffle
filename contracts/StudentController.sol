// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Storage
 * @dev Store & retrieve value in a variable
 */

contract StudentController {

    struct Student {
        string _id;
        string _name;
    }
    
    uint public nextId = 1;
    mapping(uint => Student) public student;
    
    function addStudent(string memory id, string memory name) public {
        Student memory new_student = Student(id, name);
        student[nextId] = new_student;
        nextId++;
    }
    
    function getStudent(string memory id) view public returns(string memory, string memory) {
        uint i = _find(id);
        return(student[i]._id, student[i]._name);
    }

    function getAllStudent(uint id) view public returns(string memory, string memory) {
        return(student[id]._id, student[id]._name);
    }

    // function updateStudent(bytes32 id, string name) public {
    //     uint i = _find(id);
    //     student[i]._name = name;
    // }

    function deleteStudent(string memory id) public {
        uint i = _find(id);
        delete student[i];
    }

    function _find(string memory id) view internal returns(uint) {
        for(uint i = 0; i < nextId; i++) {
            if (keccak256(abi.encodePacked((student[i]._id))) == keccak256(abi.encodePacked((id)))) {
                return i;
            }
        }
        revert('User does not exist!');
    }
}