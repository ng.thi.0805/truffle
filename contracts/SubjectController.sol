// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Storage
 * @dev Store & retrieve value in a variable
 */

contract SubjectController {
    struct Subject {
        string _id;
        string _name;
    }
    
    uint public nextId = 1;
    mapping(uint => Subject) public subject;
    Subject[] public ___subject;
    
    function addSubject(string memory id, string memory name) public {
        Subject memory new_subject = Subject(id, name);
        subject[nextId] = new_subject;
        nextId++;
    }
    
    function getSubject(string memory id) view public returns(string memory, string memory) {
        uint i = _find(id);
        return(subject[i]._id, subject[i]._name);
    }

    function getAllSubject(uint id) view public returns(string memory, string memory) {
        return(subject[id]._id, subject[id]._name);
    }

    // function updateSubject(bytes32 id, string name) public {
    //     uint i = _find(id);
    //     subject[i]._name = name;
    // }

    function deleteSubject(string memory id) public {
        uint i = _find(id);
        delete subject[i];
    }

    function _find(string memory id) view internal returns(uint) {
        for(uint i = 0; i < nextId; i++) {
            if (keccak256(abi.encodePacked((subject[i]._id))) == keccak256(abi.encodePacked((id)))) {
                return i;
            }
        }
        revert('User does not exist!');
    }
}