// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Storage
 * @dev Store & retrieve value in a variable
 */

contract TeacherController {

    struct Teacher {
        string _id;
        string _name;
    }
    
    uint public nextId = 1;
    mapping(uint => Teacher) public teacher;
    Teacher[] public ___Teacher;
    
    function addChangID(uint d) public {
        nextId = d;
    }
    
    function addTeacher(string memory id, string memory name) public {
        Teacher memory new_Teacher = Teacher(id, name);
        teacher[nextId] = new_Teacher;
        nextId++;
    }
    
    function getTeacher(string memory id) view public returns(string memory, string memory) {
        uint i = _find(id);
        return(teacher[i]._id, teacher[i]._name);
    }

    function getAllTeacher(uint id) view public returns(string memory, string memory) {
        return(teacher[id]._id, teacher[id]._name);
    }

    // function updateTeacher(bytes32 id, string name) public {
    //     uint i = _find(id);
    //     Teacher[i]._name = name;
    // }

    function deleteTeacher(string memory id) public {
        uint i = _find(id);
        delete teacher[i];
    }

    function _find(string memory id) view internal returns(uint) {
        for(uint i = 0; i < nextId; i++) {
            if (keccak256(abi.encodePacked((teacher[i]._id))) == keccak256(abi.encodePacked((id)))) {
                return i;
            }
        }
        revert('User does not exist!');
    }
}