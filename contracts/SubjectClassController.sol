// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

contract SubjectClassController {
    
    uint public nextId = 1;
    mapping(uint => string) public _id;
    mapping(uint => string) public _subject;
    mapping(uint => string) public _teacher;
    mapping(uint => uint) public _status;
    mapping(uint => uint[]) public _student;
    
    function addSubjectClass1(string memory id, string memory teacher) public {
        _id[nextId] = id;
        _teacher[nextId] = teacher;
    }
    
    function addSubjectClass2(string memory subject) public {
        _subject[nextId] = subject;
        _status[nextId] = 1;
        nextId++;
    }

    function addStudent2Class(uint id, uint student) public {
        _student[id].push(student);
    }
    
    function getSubjectClass(string memory id) view public returns(string memory, string memory, string memory, uint, uint[] memory) {
        uint i = _find(id);
        return(_id[i], _subject[i], _teacher[i], _status[i], _student[i]);
    }

    function getAllSubjectClass(uint id) view public returns(string memory, string memory, string memory, uint, uint[] memory) {
        return(_id[id], _subject[id], _teacher[id], _status[id], _student[id]);
    }

    // function updateSubjectClass(bytes32 id, string name) public {
    //     uint i = _find(id);
    //     subjectClass[i]._teacher = name;
    // }

    function _find(string memory id) view internal returns(uint) {
        for(uint i = 0; i < nextId; i++) {
            if (keccak256(abi.encodePacked(_id[i])) == keccak256(abi.encodePacked((id)))) {
                return i;
            }
        }
        revert('User does not exist!');
    }
}