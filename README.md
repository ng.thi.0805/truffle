# Yêu cầu  
* OS:   
ubuntu || linux

* nodejs:  
sudo apt install nodejs

* npm:   
sudo apt install npm

* truffle:  
sudo npm install -g truffle

# Build ứng dụng
* Tạo 1 của sổ termial (giả lập mạng lưới ethereum local)  
cd truffle  
truffle develop   

* Tạo cửa sổ termial mới  
npm install --save-dev lite-server
truffle deploy  
npm run dev  

# Contract
1. StudentController.sol  
Lưu trữ DS sinh viên (id, name)

1. SubjectController.sol  
Lưu trữ DS môn học (id, name)

1. TeacherController.sol  
Lưu trữ DS giảng viên (id, name)

1. SubjectClassController.sol  
Lưu trữ DS lớp môn (id, teacherID, subjectID, studentList)

# Mô tả:  
* truffle develop  
tạo mạng ethereum giả kèm theo tài khoản để có thể deploy các contract trong quá trình xây dựng

* truffle deploy  
Khi chạy lệnh deploy truffle sẽ gọi vào thư mục migrations và gọi vào từng file các file bắt đầu bằng số theo thứ tự từ nhỏ đến lớn.  
Các file js này được cấu hình để gọi đến các contruct và deploy lên trên mạng ethereum.  
Nếu file nào không muốn deploy thì để # hoặc 1 ký tự nào đó không phải là số.  

Sau khi deploy thành công truffle sẽ sinh ra các file json trong thư mục /build/contracts
Mỗi file trong thư mục trên sẽ có chứa thông tin của 1 smart contract (vd: transactionHash, address, block, các thông tin về function của contract)

* npm run dev  
chạy server node js (mặc định cổng 3000 | http://localhost:3000/)

# Sử dụng:

![](images/1.truffle.png)
Thêm giảng viên, sinh viên và môn học.

![](images/2.truffle.png)
Thêm lớp môn

![](images/3.truffle.png)
Thêm sinh viên vào lớp môn

# Ưu điểm:  
Smart contract giữ được hết các ưu điểm của blockchain nhưng vẫn còn tồn tại một số vấn đề.

# Vấn đề:
![](images/4.truffle.png)
Khi cài đặt smart contract trên local em gặp khá nhiều lỗi không tim ra nguyên nhân.  
Tiêu biểu là lỗi hình trên, e đã mất 3-4 ngày nhưng không thể nào tìm ra giải pháp. Trong 1 function không thể nào e sử dụng phép gán được nhiều hơn 3 lần. 
Nhưng khi test trên remix vẫn chạy bình thường.

-- 

Smart contract này chỉ phù hợp với những ứng dụng nhỏ. Dễ triển khai.
Nhưng đối với những ứng dụng lớn, cần sử lý nhiều logic thì smart contract rất không phù hợp.
Đặc biệt là những ứng dụng vd như quản lý đào tạo có rất nhiều thay đổi thì không thể nào ứng dụng smart contract vào được.
