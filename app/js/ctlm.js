var accounts, account;
var myConferenceInstance;
var myConferenceInstanceTeacher;
var myConferenceInstanceSubject;
var myConferenceInstanceSubjectClass;
var ___nextID;
var id;
var student_in_class = [];
// Initialize
function initializeConference() {
    $.getJSON('StudentController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstance = instance;
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}
function initializeConferenceTeacher() {
    $.getJSON('TeacherController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstanceTeacher = instance;
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}
function initializeConferenceSubject() {
    $.getJSON('SubjectController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstanceSubject = instance;
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}
function initializeConferenceSubjectClass() {
    $.getJSON('SubjectClassController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstanceSubjectClass = instance;
        console.log(myConferenceInstanceSubjectClass);
        getSubjectClass();
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}

// Check Values
function getAllStudent() {
	myConferenceInstance.nextId.call().then(
		function(nextId) {
            ___nextID = nextId.toNumber()
            console.log('next id student:' + ___nextID)
            return ___nextID
	}).then(
        async function(___nextID) {
            var i = 0;
            while(i<___nextID-1)
            await myConferenceInstance.getAllStudent.call(++i).then(
                function(StudentList) {
                    if (!student_in_class.includes(i)) {
                        $("#rm-sv").remove();
                        html = '';
                        html += '<tr>'
                            html += '<td>' 
                                html += StudentList[0]
                            html += '</td>'
                            html += '<td>' 
                                html += StudentList[1]
                            html += '</td>'
                            html += '<td class="text-center">' 
                                html += '<button class="btn btn-sm btn-primary them-svlm" value="' + i + '">Thêm vào lớp môn</button>'
                            html += '</td>'
                        html += '</tr>'
                        $("#dssv").append(html);
                    }
                });
        }
    );
}
function getAllTeacher() {
	myConferenceInstanceTeacher.nextId.call().then(
		function(nextId) {
            ___nextID = nextId.toNumber()
            console.log('next id teacher:' + ___nextID)
            return ___nextID
	}).then(
        function(___nextID) {
            var i = 1;
            while(i<___nextID)
            myConferenceInstanceTeacher.getAllTeacher.call(i++).then(
                function(TeacherList) {
                    $("#rm-gv").remove();
                    html = '';
                    option = '';
                    html += '<tr>'
                        html += '<td>' 
                            html += TeacherList[0]
                        html += '</td>'
                        html += '<td>' 
                            html += TeacherList[1]
                        html += '</td>'
                    html += '</tr>'
                    option = '<option value="' + TeacherList[0] + '"> ' + TeacherList[1] + '</option>'
                    $("#dsgv").append(html);
                    $("#chon_giang_vien").append(option);
            });
        }
    );
}
function getAllSubject() {
	myConferenceInstanceSubject.nextId.call().then(
		function(nextId) {
            ___nextID = nextId.toNumber()
            console.log('next id subject:' + ___nextID)
            return ___nextID
	}).then(
        function(___nextID) {
            var i = 1;
            while(i<___nextID)
            myConferenceInstanceSubject.getAllSubject.call(i++).then(
                function(SubjectList) {
                    html = '';
                    option = '';
                    html += '<tr>'
                        html += '<td>' 
                            html += SubjectList[0]
                        html += '</td>'
                        html += '<td>' 
                            html += SubjectList[1]
                        html += '</td>'
                    html += '</tr>'
                    option = '<option value="' + SubjectList[0] + '"> ' + SubjectList[1] + '</option>'
                    $("#dsmh").append(html);
                    $("#chon_mon_hoc").append(option);
            });
        }
    );
}
async function getSubjectClass() {
    myConferenceInstanceSubjectClass.getAllSubjectClass.call(id).then(function(SubjectList) {
        $("#ma_lm").html(SubjectList[0]);
        myConferenceInstanceSubject.getSubject.call(SubjectList[1]).then(function(Subject) {
            $("#ten_mh").html(Subject[1]);
        });
        myConferenceInstanceTeacher.getTeacher.call(SubjectList[2]).then(function(Teacher) {
            $("#ten_gv").html(Teacher[1]);
        });
        SubjectList[4].forEach(function(value){
            myConferenceInstance.getAllStudent.call(value.toNumber()).then(
                function(StudentList) {
                    student_in_class.push(value.toNumber())
                    $("#rm-svlm").remove();
                    html = '';
                    html += '<tr>'
                        html += '<td>' 
                            html += StudentList[0]
                        html += '</td>'
                        html += '<td>' 
                            html += StudentList[1]
                        html += '</td>'
                    html += '</tr>'
                    $("#dssvlm").append(html);
            });
        });
        getAllStudent();
    });
}

// them
async function themSVLM(ma_sv) {
    $("#rm-lm").remove();
	a = await myConferenceInstanceSubjectClass.addStudent2Class(id, ma_sv, {from: buyer_account})
    // html = ''
    // html += '<tr>'
    //     html += '<td>' 
    //         html += ma_sv
    //     html += '</td>'
    //     html += '<td>' 
    //         html += 'Vừa tạo'
    //     html += '</td>'
    // html += '</tr>'
    // $("#rm-svlm").remove();
    // $("#dssvlm").append(html);
}

window.onload = async function() {
    if (typeof web3 !== 'undefined') {
        web3Provider = web3.currentProvider;
    } else {
        web3Provider = new Web3.providers.HttpProvider('http://localhost:9545');
    }
    web3 = new Web3(web3Provider);
    accounts = web3.eth.accounts;
    organizer_account = accounts[0];
    buyer_account = accounts[1];
    console.log('Organizer account: ', organizer_account);
    console.log('Buyer account: ', buyer_account)
    await initializeConferenceTeacher();
    await initializeConference();
    await initializeConferenceSubject();
    await initializeConferenceSubjectClass();
    var url = new URL(window.location.href);
    id = url.searchParams.get("id");
    
	$(document).on('click', '.them-svlm', function() {
        themSVLM($(this).val())
        a = $(this).parent().parent().find('td')
        b = $(a[0]).text();
        c = $(a[1]).text();
        html = '';
        html += '<tr>'
            html += '<td>' 
                html += b
            html += '</td>'
            html += '<td>' 
                html += c
            html += '</td>'
        html += '</tr>'
        $("#dssvlm").append(html);
        $(this).parent().parent().remove();
    });
};