var accounts, account;
var myConferenceInstance;

function initializeConference() {
    $.getJSON('StudentController.json', function(data) {
    var StudentControllerArtifact = data;
    StudentController = TruffleContract(StudentControllerArtifact);

    // Set the provider for our contract.
    StudentController.setProvider(web3Provider);
    console.log('Here');
    console.log('Event Tickets', StudentController);
    StudentController.deployed().then(
    function(instance) {
        console.log('Event Tickets contract instance', instance);
        myConferenceInstance = instance;
        $("#confAddress").html(myConferenceInstance.address);
        console.log('The contract address', instance.address);
        checkValues();
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
    });
    });
}

// get all student
function getAllStudent() {
    // Get initial paid amount for current buyer
    myConferenceInstance.getAllStudent.call(buyerAddress)
    .then(
        function(initialPaid) {
            myConferenceInstance.getAllStudent()
            .then(
                function() {
                    console.log('Buy suceessful !!!!!!!')
                    return myConferenceInstance.numRegistrants.call();
                })
	});
}

window.onload = function() {
    if (typeof web3 !== 'undefined') {
        web3Provider = web3.currentProvider;
    } else {
        // If no injected web3 instance is detected, fall back to Ganache
        web3Provider = new Web3.providers.HttpProvider('http://localhost:9545');
    }
    web3 = new Web3(web3Provider);
    console.log(web3);
    console.log('Getting development accounts');
    accounts = web3.eth.accounts;
    organizer_account = accounts[0];
    buyer_account = accounts[1];
    console.log('Organizer account: ', organizer_account);
    console.log('Buyer account: ', buyer_account)
    console.log('Initializing Event Tickets DApp')
    initializeConference();
    getAllStudent();
	// Wire up the UI elements
	$("#changeQuota").click(function() {
		var val = $("#confQuota").val();
		changeQuota(val);
	});

	$("#buyTicket").click(function() {
		var val = $("#ticketPrice").val();
		var buyerAddress = $("#buyerAddress").val();
		buyTicket(buyerAddress, web3.toWei(val));
	});

	$("#refundTicket").click(function() {
		var val = $("#refundAmount").val();
		var buyerAddress = $("#refBuyerAddress").val();
		refundTicket(buyerAddress, web3.toWei(val));
	});

    $("#destroy").click(function() {
        destroy();
    });

	// Set value of wallet to accounts[1]
	$("#buyerAddress").val(buyer_account);
	$("#refBuyerAddress").val(buyer_account);

};
