var accounts, account;
var myConferenceInstance;
var myConferenceInstanceTeacher;
var myConferenceInstanceSubject;
var myConferenceInstanceSubjectClass;
var ___nextID;

// Initialize
function initializeConference() {
    $.getJSON('StudentController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstance = instance;
        getAllStudent();
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}
function initializeConferenceTeacher() {
    $.getJSON('TeacherController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstanceTeacher = instance;
        getAllTeacher();
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}
function initializeConferenceSubject() {
    $.getJSON('SubjectController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstanceSubject = instance;
        getAllSubject();
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}
function initializeConferenceSubjectClass() {
    $.getJSON('SubjectClassController.json', function(data) {
    var EventTicketsArtifact = data;
    EventTickets = TruffleContract(EventTicketsArtifact);
    EventTickets.setProvider(web3Provider);
    EventTickets.deployed().then(
    function(instance) {
        myConferenceInstanceSubjectClass = instance;
        console.log(myConferenceInstanceSubjectClass);
        getAllSubjectClass();
    }).catch(
        function(err) {
            console.log('Contract is not deployed???');
            console.log(err);
        });
    });
}

// Check Values
function getAllStudent() {
	myConferenceInstance.nextId.call().then(
		function(nextId) {
            ___nextID = nextId.toNumber()
            console.log('next id student:' + ___nextID)
            return ___nextID
	}).then(
        function(___nextID) {
            var i = 1;
            while(i<___nextID)
            myConferenceInstance.getAllStudent.call(i++).then(
                function(StudentList) {
                    $("#rm-sv").remove();
                    html = '';
                    html += '<tr>'
                        html += '<td>' 
                            html += StudentList[0]
                        html += '</td>'
                        html += '<td>' 
                            html += StudentList[1]
                        html += '</td>'
                    html += '</tr>'
                    $("#dssv").append(html);
            });
        }
    );
}
function getAllTeacher() {
	myConferenceInstanceTeacher.nextId.call().then(
		function(nextId) {
            ___nextID = nextId.toNumber()
            console.log('next id teacher:' + ___nextID)
            return ___nextID
	}).then(
        function(___nextID) {
            var i = 1;
            while(i<___nextID)
            myConferenceInstanceTeacher.getAllTeacher.call(i++).then(
                function(TeacherList) {
                    $("#rm-gv").remove();
                    html = '';
                    option = '';
                    html += '<tr>'
                        html += '<td>' 
                            html += TeacherList[0]
                        html += '</td>'
                        html += '<td>' 
                            html += TeacherList[1]
                        html += '</td>'
                    html += '</tr>'
                    option = '<option value="' + TeacherList[0] + '"> ' + TeacherList[1] + '</option>'
                    $("#dsgv").append(html);
                    $("#chon_giang_vien").append(option);
            });
        }
    );
}
function getAllSubject() {
	myConferenceInstanceSubject.nextId.call().then(
		function(nextId) {
            ___nextID = nextId.toNumber()
            console.log('next id subject:' + ___nextID)
            return ___nextID
	}).then(
        function(___nextID) {
            var i = 1;
            $("#rm-mh").remove();
            while(i<___nextID)
            myConferenceInstanceSubject.getAllSubject.call(i++).then(
                function(SubjectList) {
                    html = '';
                    option = '';
                    html += '<tr>'
                        html += '<td>' 
                            html += SubjectList[0]
                        html += '</td>'
                        html += '<td>' 
                            html += SubjectList[1]
                        html += '</td>'
                    html += '</tr>'
                    option = '<option value="' + SubjectList[0] + '"> ' + SubjectList[1] + '</option>'
                    $("#dsmh").append(html);
                    $("#chon_mon_hoc").append(option);
            });
        }
    );
}
function getAllSubjectClass() {
	myConferenceInstanceSubjectClass.nextId.call().then(
		function(nextId) {
            ___nextID = nextId.toNumber()
            console.log('next id subject class:' + ___nextID)
            return ___nextID
	}).then(
        function(___nextID) {
            var i = 0;
            $("#rm-lm").remove();
            while(i<___nextID-1)
            myConferenceInstanceSubjectClass.getAllSubjectClass.call(++i).then(
                function(SubjectList) {
                    console.log(SubjectList)
                    html = '';
                    option = '';
                    html += '<tr>'
                        html += '<td>' 
                            html += SubjectList[0]
                        html += '</td>'
                        html += '<td>' 
                            html += SubjectList[1]
                        html += '</td>'
                        html += '<td>' 
                            html += SubjectList[2]
                        html += '</td>'
                        html += '<td>' 
                            html += SubjectList[3].toNumber() == 1 ? 'Vừa tạo' : SubjectList[3] == 2 ? 'Đang học' : 'Kết thúc' 
                        html += '</td>'
                        html += '<td>' 
                            html += '<a href="/chi-tiet-lop-mon.html?id=' + i + '">Chi tiết</a>'
                        html += '</td>'
                    html += '</tr>'
                    option = '<option value="' + SubjectList[0] + '"> ' + SubjectList[1] + '</option>'
                    $("#dslm").append(html);
            });
        }
    );
}

// them sinh vien
function themSV(ma_sv, ten_sv) {
    $("#rm-sv").remove();
	myConferenceInstance.addStudent(ma_sv, ten_sv, {from: organizer_account})
    html = ''
    html += '<tr>'
        html += '<td>' 
            html += ma_sv
        html += '</td>'
        html += '<td>' 
            html += ten_sv
        html += '</td>'
    html += '</tr>'
    $("#dssv").append(html);
}
function themGV(ma_gv, ten_gv) {
    $("#rm-gv").remove();
	myConferenceInstanceTeacher.addTeacher(ma_gv, ten_gv, {from: organizer_account})
    html = ''
    html += '<tr>'
        html += '<td>' 
            html += ma_gv
        html += '</td>'
        html += '<td>' 
            html += ten_gv
        html += '</td>'
    html += '</tr>'
    $("#dsgv").append(html);
}
function themMH(ma_mh, ten_mh) {
    $("#rm-mh").remove();
	myConferenceInstanceSubject.addSubject(ma_mh, ten_mh, {from: organizer_account})
    html = ''
    html += '<tr>'
        html += '<td>' 
            html += ma_mh
        html += '</td>'
        html += '<td>' 
            html += ten_mh
        html += '</td>'
    html += '</tr>'
    $("#dsmh").append(html);
}
async function themLM(ma_lm, ma_gv, ma_mh) {
    $("#rm-lm").remove();
	await myConferenceInstanceSubjectClass.addSubjectClass1(ma_lm, ma_gv, {from: buyer_account})
	await myConferenceInstanceSubjectClass.addSubjectClass2(ma_mh, {from: buyer_account})
    html = ''
    html += '<tr>'
        html += '<td>' 
            html += ma_lm
        html += '</td>'
        html += '<td>' 
            html += ma_mh
        html += '</td>'
        html += '<td>' 
            html += ma_gv
        html += '</td>'
        html += '<td>' 
            html += 'Vừa tạo'
        html += '</td>'
    html += '</tr>'
    $("#dslm").append(html);
}

window.onload = function() {
    if (typeof web3 !== 'undefined') {
        web3Provider = web3.currentProvider;
    } else {
        web3Provider = new Web3.providers.HttpProvider('http://localhost:9545');
    }
    web3 = new Web3(web3Provider);
    accounts = web3.eth.accounts;
    organizer_account = accounts[0];
    buyer_account = accounts[1];
    console.log('Organizer account: ', organizer_account);
    console.log('Buyer account: ', buyer_account)
    initializeConference();
    initializeConferenceTeacher();
    initializeConferenceSubject();
    initializeConferenceSubjectClass();
	// Wire up the UI elements
	$("#them_sv").click(function() {
		var ten_sv = $("#ten_sv").val();
		var ma_sv = $("#ma_sv").val();
		themSV(ma_sv, ten_sv);
	});
	$("#them_mh").click(function() {
		var ten_mh = $("#ten_mh").val();
		var ma_mh = $("#ma_mh").val();
		themMH(ma_mh, ten_mh);
	});
	$("#them_gv").click(function() {
		var ten_gv = $("#ten_gv").val();
		var ma_gv = $("#ma_gv").val();
		themGV(ma_gv, ten_gv);
	});
	$("#them_lop_mon").click(function() {
		var ma_lm = $("#ma_lm").val();
		var ma_gv = $("#chon_giang_vien").val();
		var ma_mh = $("#chon_mon_hoc").val();
		themLM(ma_lm, ma_gv, ma_mh);
	});

};